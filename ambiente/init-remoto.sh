#!/bin/sh

PROJECT_REPO_USER=
ARTIFACT_HOST_USER=
QUALITY_HOST_USER=

PROJECT_REPO_HOST=https://gitlab.com/$PROJECT_REPO_USER/conchayoro.git
PROJECT_ENV_PATH=/root/conchayoro/ambiente
DOCKER_FILE=docker-compose-image-pre-build.yml

echo "===> Configuração Jenkins"
docker-compose -f $DOCKER_FILE --compatibility up -d jenkins

echo "===> Configuração Nexus"
ssh -o "StrictHostKeyChecking no" -t $ARTIFACT_HOST_USER "git clone $PROJECT_REPO_HOST"
scp .credenciais .env $ARTIFACT_HOST_USER:$PROJECT_ENV_PATH
ssh -o "StrictHostKeyChecking no" -t $ARTIFACT_HOST_USER "cd $PROJECT_ENV_PATH && docker-compose -f $DOCKER_FILE --compatibility up -d nexus && docker-compose exec nexus sh /deploy-config/nexus/init.sh"

echo "===> Configuração Sonar"
ssh -o "StrictHostKeyChecking no" -t $QUALITY_HOST_USER "git clone $PROJECT_REPO_HOST"
scp .credenciais .env $QUALITY_HOST_USER:$PROJECT_ENV_PATH
ssh -o "StrictHostKeyChecking no" -t $QUALITY_HOST_USER "cd $PROJECT_ENV_PATH && docker-compose -f $DOCKER_FILE --compatibility up -d sonar"

echo "===> Configuração do ambiente remoto concluída com sucesso!!"